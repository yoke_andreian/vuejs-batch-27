// soal 1
var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 },
    { name: 'komik', timeSpent: 1000 }
]

// books.forEach(e => readBooks(10000, e, (callback) => {
//     console.log(callback)
// }))

function baca(time, book, i) {
    if (i < book.length) {
        readBooks(time, book[i], function (sisa) {
            if (sisa > 0) {
                i += 1;
                baca(sisa, book, i);
            }
        })
    }
}

baca(1000, books, 0);

// soal ke 2 masih bingung.