const posts = [
    {
        title: "Post one",
        body: 'This is post one'
    },
    {
        title: "Post two",
        body: 'This is post two'
    }
]

const createPost = post => {
    setTimeout(() => {
        posts.push(post)
    }, 2000)
}

const getPosts = () => {
    setTimeout(() => {
        posts.forEach(post => {
            console.log(post);
        })
    }, 1000);
}

const newPost = {
    title: 'Post tree',
    body: 'This is post tree'
}

createPost(newPost);
getPosts();