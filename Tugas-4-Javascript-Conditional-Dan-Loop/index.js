// soal 1
var nilai = 80;

if (nilai >= 85) {
    console.log('indeksnya A');
} else if(nilai >=75 && nilai < 85 ){
    console.log('indeksnya B');
} else if (nilai >= 65 && nilai < 75) {
    console.log('indeksnya C');
} else if (nilai >= 55 && nilai < 65) {
    console.log('indeksnya D');
} else {
    console.log('indeksnya E');
}


// soal 2
var tanggal = 16;
var bulan = 5;
var tahun = 1991;
switch (bulan) {
    case 1:
        console.log(tanggal + ' January ' + tahun);
        break;
 
    case 2:
        console.log(tanggal + ' February ' + tahun);
        break;

    case 3:
        console.log(tanggal + ' Maret ' + tahun);
        break;

    case 4:
        console.log(tanggal + ' April ' + tahun);
        break;

    case 5:
        console.log(tanggal + ' Mei ' + tahun);
        break;

    case 6:
        console.log(tanggal + ' Juni ' + tahun);
        break;

    case 7:
        console.log(tanggal + ' Juli ' + tahun);
        break;

    case 8:
        console.log(tanggal + ' Agustus ' + tahun);
        break;

    case 9:
        console.log(tanggal + ' September ' + tahun);
        break;

    case 10:
        console.log(tanggal + ' Oktober ' + tahun);
        break;

    case 11:
        console.log(tanggal + ' November ' + tahun);
        break;

    case 12:
        console.log(tanggal + ' Desember ' + tahun);
        break;
        
    default:
        console.log('Bulan tidak sesui');
}

// soal ke 3
// output n = 3
var result = '';
var n = 3;
for (var i = 0; i < n; i++){
    for (var j = 0; j <= i; j++) {
        result += '#'
    }
    result += '\n';
}
// output m = 7
var hasil = '';
var m = 7;
for (var i = 0; i < m; i++){
    for (var j = 0; j <= i; j++) {
        hasil += '#'
    }
    hasil += '\n';
}

// soal ke 4
var l = 3;
for (var a = 1; a < l; a++ ) {
    if (a == 0) {
        console.log("1 - I love programming");
    } else if (a == 1) {
        console.log("2 - I love Javascript");
    } else if (a == 2) {
        console.log("3 - I love VueJS");
    } else {
        console.log('=======');
    }
}

var l = 5;
for (var a = 1; a < l; a++ ) {
    for (var b = 1; b <= a; b++ ){
        if (a == 1) {
            console.log(b + " - I love programming");
        } else if (a == 2) {
            console.log(b + " - I love Javascript");
        } else if (a == 3) {
            console.log(b + " - I love VueJS");
        } else {
            console.log('=======');
        }
    }
}