// soal 1
let luasPersegiPanjang = (p, l) => { return p * l }
let kelilingPersegiPanjang = (p, l) => { return 2(p + l) }

console.log(luasPersegiPanjang(5, 6));
console.log(kelilingPersegiPanjang(5, 6));

// soal 2
const newFunction = (firstName, lastName) => (
    {
        firstName: firstName,
        lastName: lastName,
        fullName: function () {
            console.log(firstName + " " + lastName)
        }
    }

);

//Driver Code 
newFunction("William", "Imoh").fullName()

// soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

let { firstname, lastName, address, hobby } = newObject;

// console.log(firstname);

// // soal ke 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combinedArray = [...west, ...east];
//Driver Code
console.log(combinedArray)

const planet = "earth"
const view = "glass"
let before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet

let after = `Lorem ${view} dolot sit amet, consectetur adipiscing elit, ${planet}`;

console.log(after);