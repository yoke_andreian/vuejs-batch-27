// soal ke 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

var result = daftarHewan.sort();

console.log(result);

// soale ke 2
function introduce (data) {
    console.log(
        "Nama saya "+data['name']+", umur saya "+data['age']+" tahun, alamat saya di "+data['address']+", dan saya punya "+data['hobby']+" yaitu Gaming"
    );    
}
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan)

// soal ke 3
function hitung_huruf_vokal(data) {
    var result = data.length;
    return result;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log([hitung_1,hitung_2]);